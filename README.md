# Skylark
[![pipeline status](https://gitlab.com/skylark-xmpp/skylark/badges/develop/pipeline.svg)](https://gitlab.com/skylark-xmpp/skylark/-/commits/develop) 
[![coverage report](https://gitlab.com/skylark-xmpp/skylark/badges/develop/coverage.svg)](https://gitlab.com/skylark-xmpp/skylark/-/commits/develop) 

A modern, web-based, fully in-browser XMPP client.

## Usage
A very early-in-development version is available at https://skylark.mewmew.moe/

## Compliance
Overview of XMPP compliance in accordance to [XEP-0479].

| Category | Level    | Feature                                       | Standards                                      | Status  |
| -------- | -------- | --------------------------------------------- | ---------------------------------------------- | ------- |
| Core     | Core     | Core features                                 | [RFC6120]                                      | ⏳      |
| Core     | Core     | TLS                                           | [RFC7590]                                      | ✅      |
| Core     | Core     | Feature discovery                             | [XEP-0030]                                     | ⏳      |
| Core     | Core     | Feature broadcasts                            | [XEP-0115]                                     | ✅      |
| Web      | Core     | Web Connection Mechanisms                     | [RFC7395]                                      | ✅      |
| Web      | Core     | Connection Mechanism Discovery                | [XEP-0156]                                     | ✅      | 
| IM       | Core     | Core features                                 | [RFC6121]                                      | ⏳ (#9) |
| IM       | Core     | The /me command                               | [XEP-0245]                                     | ✅      |
| IM       | Core     | vcard-temp                                    | [XEP-0054]                                     | ❌ (#3) |
| IM       | Core     | Outbound Message Synchronization              | [XEP-0280]                                     | ❌ (#2) |
| IM       | Core     | Group Chat                                    | [XEP-0045], [XEP-0249]                         | ❌ (#4) |
| IM       | Core     | File Upload                                   | [XEP-0363]                                     | ❌ (#5) |
| Core     | Advanced | Event publishing                              | [XEP-0163]                                     | ❌      |
| IM       | Advanced | User Avatars                                  | [XEP-0084]                                     | ❌      |
| IM       | Advanced | User Avatar Compatibility                     | [XEP-0398], [XEP-0153]                         | ❌      |
| IM       | Advanced | User Blocking                                 | [XEP-0191]                                     | ❌      |
| IM       | Advanced | Advanced Group Chat                           | [XEP-0048], [XEP-0313], [XEP-0402], [XEP-0410] | ❌      |
| IM       | Advanced | Persistent Storage of Private Data via PubSub | [XEP-0223]                                     | ❌      |
| IM       | Advanced | Private XML Storage                           | [XEP-0049]                                     | ❌      |
| IM       | Advanced | Stream Management                             | [XEP-0198]                                     | ❌      |
| IM       | Advanced | Message Acknowledgements                      | [XEP-0184]                                     | ❌      |
| IM       | Advanced | History Storage / Retrieval                   | [XEP-0313]                                     | ❌ (#7) |
| IM       | Advanced | Chat States                                   | [XEP-0085]                                     | ❌      |
| IM       | Advanced | Message Correction                            | [XEP-0308]                                     | ❌      |
| IM       | Advanced | Direct File Transfer                          | [XEP-0234], [XEP-0261]                         | ❌      |

## Other features
- Message styling ([XEP-0393])

## License
Skylark is licensed under the [Mozilla Public License version 2.0](https://www.mozilla.org/en-US/MPL/2.0/).

[RFC6120]: https://datatracker.ietf.org/doc/html/rfc6120 "Extensible Messaging and Presence Protocol (XMPP): Core"
[RFC6121]: https://datatracker.ietf.org/doc/html/rfc6121 "Extensible Messaging and Presence Protocol (XMPP): Instant Messaging and Presence"
[RFC7590]: https://datatracker.ietf.org/doc/html/rfc7590 "Use of Transport Layer Security (TLS) in the Extensible Messaging and Presence Protocol (XMPP)"
[RFC7395]: https://datatracker.ietf.org/doc/html/rfc7395 "An Extensible Messaging and Presence Protocol (XMPP) Subprotocol for WebSocket"
[XEP-0030]: https://xmpp.org/extensions/xep-0030.html "Service Discovery"
[XEP-0045]: https://xmpp.org/extensions/xep-0045.html "Multi-User Chat"
[XEP-0048]: https://xmpp.org/extensions/xep-0048.html "Bookmarks"
[XEP-0049]: https://xmpp.org/extensions/xep-0049.html "Private XML Storage"
[XEP-0054]: https://xmpp.org/extensions/xep-0054.html "vcard-temp"
[XEP-0084]: https://xmpp.org/extensions/xep-0084.html "User Avatar"
[XEP-0085]: https://xmpp.org/extensions/xep-0085.html "Chat State Notifications"
[XEP-0115]: https://xmpp.org/extensions/xep-0115.html "Entity Capabilities"
[XEP-0153]: https://xmpp.org/extensions/xep-0153.html "vCard-Based Avatars"
[XEP-0156]: https://xmpp.org/extensions/xep-0156.html "Discovering Alternative XMPP Connection Methods"
[XEP-0163]: https://xmpp.org/extensions/xep-0163.html "Personal Eventing Protocol"
[XEP-0184]: https://xmpp.org/extensions/xep-0184.html "Message Delivery Receipts"
[XEP-0191]: https://xmpp.org/extensions/xep-0191.html "Blocking Command"
[XEP-0198]: https://xmpp.org/extensions/xep-0198.html "Stream Management"
[XEP-0223]: https://xmpp.org/extensions/xep-0223.html "Persistent Storage of Private Data via PubSub"
[XEP-0234]: https://xmpp.org/extensions/xep-0234.html "Jingle File Transfer"
[XEP-0245]: https://xmpp.org/extensions/xep-0245.html "The /me Command"
[XEP-0249]: https://xmpp.org/extensions/xep-0249.html "Direct MUC Invitations"
[XEP-0261]: https://xmpp.org/extensions/xep-0261.html "Jingle In-Band Bytestreams Transport Method"
[XEP-0280]: https://xmpp.org/extensions/xep-0280.html "Message Carbons"
[XEP-0308]: https://xmpp.org/extensions/xep-0308.html "Last Message Correction"
[XEP-0313]: https://xmpp.org/extensions/xep-0313.html "Message Archive Management"
[XEP-0363]: https://xmpp.org/extensions/xep-0363.html "HTTP File Upload"
[XEP-0393]: https://xmpp.org/extensions/xep-0393.html "Message Styling"
[XEP-0398]: https://xmpp.org/extensions/xep-0398.html "User Avatar to vCard-Based Avatars Conversion"
[XEP-0402]: https://xmpp.org/extensions/xep-0402.html "PEP Native Bookmarks"
[XEP-0410]: https://xmpp.org/extensions/xep-0410.html "MUC Self-Ping (Schrödinger's Chat)"
[XEP-0479]: https://xmpp.org/extensions/xep-0479.html "XMPP Compliance Suites 2023"
