import { SkylarkJSX } from "./jsx";
import { IXmppTransport } from "./transports";
import { PresenceStanza } from "./types";
import { AuthParameters, ISaslAuthenticator, saslNegotiate } from "./sasl";
import { IBaseModule } from "./plumbing/module";
import {
  ComponentRegistry,
  IQ_HANDLER,
  MESSAGE_HANDLER,
  PRESENCE_EXTENSION,
  RegisteredIqHandler,
} from "./plumbing/registry";
import { JID } from "@skylark/jid";
import { StanzaError } from "./errors";

interface PendingIqHandler {
  id: string;
  resolve: (doc: Element | null) => void;
  reject: () => void;
}

export interface ConnectionCallbacks {
  streamOpen?: () => void;
  messageReceived?: (el: Element) => void;
  presenceReceived?: (el: Element) => void;
}

export class Connection extends EventTarget {
  transport: IXmppTransport;
  xmlSerializer = new XMLSerializer();

  serverDomain: string;

  isAuthenticated = false;
  nextIqIdentifier = 1;
  pendingIqs: PendingIqHandler[] = [];
  saslAuthenticator?: ISaslAuthenticator;
  saslHandler?: {
    success: () => void;
    failure: (error?: Element) => void;
  };
  registry = new ComponentRegistry();

  constructor(
    transport: IXmppTransport,
    private authParameters: AuthParameters,
    modules: IBaseModule[]
  ) {
    super();

    for (const module of modules) {
      module.register && module.register(this.registry);
    }
    for (const module of modules) {
      module.postRegister && module.postRegister(this.registry);
    }

    this.transport = transport;
    this.serverDomain = authParameters.userJid.domain;
    transport.onStanzaReceived = this._onWsMessage.bind(this);
  }

  connect(): Promise<void> {
    return this.transport.openStream(this.serverDomain);
  }

  _restartStream() {
    this.transport
      .openStream(this.serverDomain)
      .then(() => {
        console.info("XMPP stream open");
      })
      .catch((err) => console.error("Failed to open XMPP stream", err));
  }

  _getIqIdentifier(): string {
    const id = String(this.nextIqIdentifier);
    this.nextIqIdentifier++;
    return id;
  }

  async performSasl(
    mechanism: string,
    authenticator: ISaslAuthenticator
  ): Promise<void> {
    const initialResponse = await authenticator.initiate();
    const initialResponseEncoded =
      initialResponse !== null
        ? btoa(String.fromCharCode(...Array.from(initialResponse)))
        : "=";
    this.saslAuthenticator = authenticator;
    this._sendXml(
      <auth xmlns="urn:ietf:params:xml:ns:xmpp-sasl" mechanism={mechanism}>
        {initialResponseEncoded}
      </auth>
    );
    return new Promise((resolve, reject) => {
      this.saslHandler = {
        success: resolve,
        failure: () => {
          reject(new Error("Authentication failed"));
        },
      };
    });
  }

  iq(
    type: "get" | "set",
    payload: SkylarkJSX.Element,
    options: { to?: string } = {}
  ): Promise<Element | null> {
    const iqId = this._getIqIdentifier();
    const iqPromise = new Promise<Element | null>((resolve, reject) => {
      this.pendingIqs.push({
        id: iqId,
        resolve,
        reject,
      });
    });
    this._sendXml(
      <iq xmlns="jabber:client" type={type} id={iqId} to={options.to}>
        {payload}
      </iq>
    );
    return iqPromise;
  }

  presence(options: { to?: JID; type?: string } = {}) {
    const extensions = this.registry
      .get<() => SkylarkJSX.Element>(PRESENCE_EXTENSION)
      .map((ex) => ex());
    this._sendXml(
      <presence
        xmlns="jabber:client"
        to={options.to?.toString()}
        type={options.type}
      >
        {extensions}
      </presence>
    );
  }

  _sendXml(elem: SkylarkJSX.Element) {
    const doc = document.implementation.createDocument(null, null);
    const domElem = SkylarkJSX.toDOM(doc, elem);
    doc.appendChild(domElem);
    const payload = this.xmlSerializer.serializeToString(doc);
    this.transport.sendStanza(payload);
  }

  _onWsMessage(elem: Element) {
    if (
      elem.localName === "features" &&
      elem.namespaceURI === "http://etherx.jabber.org/streams"
    ) {
      console.group("Received stream features");
      for (const element of elem.children) {
        console.log(element.outerHTML);
      }
      console.groupEnd();

      if (!this.isAuthenticated) {
        const saslFeature = Array.from(elem.children).find(
          (el) =>
            el.localName === "mechanisms" &&
            el.namespaceURI === "urn:ietf:params:xml:ns:xmpp-sasl"
        );
        if (!saslFeature) {
          console.error(
            "Missing MtN feature: urn:ietf:params:xml:ns:xmpp-sasl"
          );
          // do something
          return;
        }

        const mechanisms = Array.from(saslFeature.children)
          .filter(
            (el) =>
              el.localName === "mechanism" &&
              el.namespaceURI === "urn:ietf:params:xml:ns:xmpp-sasl"
          )
          .map((el) => el.textContent ?? "");

        let mechanism;
        try {
          mechanism = saslNegotiate(mechanisms);
        } catch (e) {
          console.error(e);
          return;
        }
        this.performSasl(
          mechanism.mechanism,
          mechanism.factory(this.authParameters)
        ).catch((e) => {
          console.error(e);
        });
        return;
      }

      const event = new Event("streamOpen");
      this.dispatchEvent(event);
    }
    if (elem.namespaceURI === "urn:ietf:params:xml:ns:xmpp-sasl") {
      if (elem.localName === "success" && this.saslAuthenticator) {
        let response = null;
        if (elem.textContent === "=") {
          response = new Uint8Array();
        } else if (elem.textContent && elem.textContent.length) {
          response = new Uint8Array(
            atob(elem.textContent)
              .split("")
              .map((ch) => ch.charCodeAt(0))
          );
        }

        this.saslAuthenticator
          .verify(response)
          .then(() => {
            this.saslHandler?.success();
            this.saslHandler = undefined;
            this.saslAuthenticator = undefined;

            this.isAuthenticated = true;
            this._restartStream();
          })
          .catch((e) => {
            console.error("Failed to verify the server SASL response", e);
            this.saslHandler?.failure();
            this.saslHandler = undefined;
            this.saslAuthenticator = undefined;

            this.transport.closeStream();
          });
      } else if (elem.localName === "failure") {
        this.saslHandler?.failure(elem);
        this.saslHandler = undefined;
        this.saslAuthenticator = undefined;

        this.transport.closeStream();
      } else if (elem.localName === "challenge" && this.saslAuthenticator) {
        const challenge = new Uint8Array(
          atob(elem.textContent ?? "")
            .split("")
            .map((ch) => ch.charCodeAt(0))
        );
        this.saslAuthenticator.respond(challenge).then((response) => {
          const responseEncoded =
            response !== null
              ? btoa(String.fromCharCode(...Array.from(response)))
              : "=";
          this._sendXml(
            <response xmlns="urn:ietf:params:xml:ns:xmpp-sasl">
              {responseEncoded}
            </response>
          );
        });
      }
    }
    if (elem.localName === "iq" && elem.namespaceURI === "jabber:client") {
      const iqTo = elem.getAttribute("to");
      const iqFrom = elem.getAttribute("from");
      const iqType = elem.getAttribute("type");
      const iqId = elem.getAttribute("id");
      if (iqType === "get" || iqType === "set") {
        const iqHandlers = this.registry.get<RegisteredIqHandler>(IQ_HANDLER);
        const thisIqHandler = iqHandlers.find(
          (h) => h.namespaceURI === elem.children[0].namespaceURI
        );
        if (thisIqHandler) {
          try {
            const resp = thisIqHandler.handler(iqType, elem.children[0]);
            this._sendXml(
              <iq
                xmlns="jabber:client"
                id={iqId}
                from={iqTo}
                to={iqFrom}
                type="result"
              >
                {resp}
              </iq>
            );
          } catch (e) {
            if (e instanceof StanzaError) {
              this._sendXml(
                <iq
                  xmlns="jabber:client"
                  id={iqId}
                  from={iqTo}
                  to={iqFrom}
                  type="error"
                >
                  {e.toXML()}
                </iq>
              );
            } else {
              console.error("Unexpected error handling an IQ request", e);
              this._sendXml(
                <iq
                  xmlns="jabber:client"
                  id={iqId}
                  from={iqTo}
                  to={iqFrom}
                  type="error"
                >
                  <error type="cancel">
                    <internal-server-error xmlns="urn:ietf:params:xml:ns:xmpp-stanzas" />
                  </error>
                </iq>
              );
            }
          }
        } else {
          this._sendXml(
            <iq
              xmlns="jabber:client"
              id={iqId}
              from={iqTo}
              to={iqFrom}
              type="error"
            >
              <error type="cancel">
                <service-unavailable xmlns="urn:ietf:params:xml:ns:xmpp-stanzas" />
              </error>
            </iq>
          );
        }
      } else if (iqType === "result" || iqType === "error") {
        const pendingIqIndex = this.pendingIqs.findIndex((e) => e.id === iqId);
        if (pendingIqIndex === -1) return;
        const [pendingIq] = this.pendingIqs.splice(pendingIqIndex, 1);

        if (iqType === "result") {
          pendingIq.resolve(elem.firstElementChild);
        } else if (iqType === "error") {
          pendingIq.reject();
        }
      } else {
        this._sendXml(
          <iq
            xmlns="jabber:client"
            id={iqId}
            from={iqTo}
            to={iqFrom}
            type="error"
          >
            <error type="modify">
              <bad-request xmlns="urn:ietf:params:xml:ns:xmpp-stanzas" />
            </error>
          </iq>
        );
      }
    }
    if (elem.localName === "message" && elem.namespaceURI === "jabber:client") {
      const handlers =
        this.registry.get<(elem: Element) => boolean>(MESSAGE_HANDLER);
      for (const handler of handlers) {
        if (handler(elem)) {
          return;
        }
      }

      console.log("Received message", elem.outerHTML);

      const event = new CustomEvent<Element>("message", {
        detail: elem,
      });
      this.dispatchEvent(event);
    }
    if (
      elem.localName === "presence" &&
      elem.namespaceURI === "jabber:client"
    ) {
      const presence = PresenceStanza.fromDOM(elem);
      console.log("Received presence", presence);
    }
  }
}
