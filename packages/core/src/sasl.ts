import { JID } from "@skylark/jid";

export interface AuthParameters {
  userJid: JID;
  password: string;
}

export interface ISaslAuthenticator {
  initiate(): Promise<Uint8Array | null>;
  respond(challenge: Uint8Array | null): Promise<Uint8Array | null>;
  verify(serverResponse: Uint8Array | null): Promise<void>;
}

export class PlainAuthenticator implements ISaslAuthenticator {
  constructor(private params: AuthParameters) {}

  initiate(): Promise<Uint8Array | null> {
    const encoder = new TextEncoder();
    const encodedCid = encoder.encode(this.params.userJid.toBareString());
    const encodedPasswd = encoder.encode(this.params.password);
    const buffer = new Uint8Array(
      encodedCid.byteLength + encodedPasswd.byteLength + 2
    );
    buffer.set(encodedCid, 1);
    buffer.set(encodedPasswd, 1 + encodedCid.byteLength + 1);
    return Promise.resolve(buffer);
  }

  respond(_challenge: Uint8Array | null): Promise<Uint8Array | null> {
    return Promise.reject(
      new Error(
        "PLAIN SASL mechanism does not support challenge-response sequences"
      )
    );
  }

  async verify(serverResponse: Uint8Array | null): Promise<void> {
    if (serverResponse !== null) {
      throw new Error(
        "PLAIN SASL mechanism does not support server response verification"
      );
    }
  }
}

export type NonceGenerator = () => string;

export function defaultNonceGenerator(): string {
  const buffer = new Uint8Array(32);
  crypto.getRandomValues(buffer);
  return Array.from(buffer)
    .map((b) => b.toString(16).padStart(2, "0"))
    .join("");
}

export class ScramAuthenticator implements ISaslAuthenticator {
  private clientNonce?: string;
  private initialMessage?: Uint8Array;
  private serverSignature?: Uint8Array;

  constructor(
    private params: AuthParameters,
    private hashType: string,
    private hashLength: number,
    private generateNonce: NonceGenerator = defaultNonceGenerator
  ) {}

  get hashParameters() {
    return {
      hashType: this.hashType,
      hashLength: this.hashLength,
    };
  }

  initiate(): Promise<Uint8Array | null> {
    const encoder = new TextEncoder();
    this.clientNonce = this.generateNonce();
    const buffer = encoder.encode(
      `n,,n=${this.params.userJid.local},r=${this.clientNonce}`
    );
    this.initialMessage = buffer.subarray(3);
    return Promise.resolve(buffer);
  }

  async respond(challenge: Uint8Array | null): Promise<Uint8Array | null> {
    if (!challenge) {
      throw new Error("SCRAM SASL mechanism does not support empty challenges");
    }
    if (!this.initialMessage || !this.clientNonce) {
      throw new Error("Initial message not sent");
    }
    const decoder = new TextDecoder();
    let serverNonce, salt, iterations;
    const fields = decoder
      .decode(challenge)
      .split(",")
      .map((s) => {
        const splits = s.split("=");
        return [splits[0], splits.slice(1).join("=")];
      });
    for (const [key, value] of fields) {
      if (key === "r") {
        serverNonce = value;
      } else if (key === "s") {
        salt = new Uint8Array(
          atob(value)
            .split("")
            .map((ch) => ch.charCodeAt(0))
        );
      } else if (key === "i") {
        iterations = Number(value);
      }
    }
    if (!serverNonce || !salt || !iterations) {
      throw new Error("Server sent invalid challenge");
    }

    if (!serverNonce.startsWith(this.clientNonce)) {
      throw new Error("Nonce mismatch");
    }

    const clientMessageBare = new TextEncoder().encode(
      `c=biws,r=${serverNonce}`
    );

    const cryptoPassword = await crypto.subtle.importKey(
      "raw",
      new TextEncoder().encode(this.params.password),
      "PBKDF2",
      false,
      ["deriveKey"]
    );
    const saltedPassword = await crypto.subtle.deriveKey(
      {
        name: "PBKDF2",
        hash: this.hashType,
        salt,
        iterations,
      },
      cryptoPassword,
      { name: "HMAC", hash: this.hashType, length: this.hashLength },
      false,
      ["sign"]
    );
    const clientKey = await crypto.subtle.sign(
      { name: "HMAC" },
      saltedPassword,
      new TextEncoder().encode("Client Key")
    );
    const storedKey = await crypto.subtle.digest(this.hashType, clientKey);
    const authMessage = new Uint8Array(
      this.initialMessage.length +
        1 +
        challenge.length +
        1 +
        clientMessageBare.length
    );
    authMessage.set(this.initialMessage, 0);
    authMessage[this.initialMessage.length] = 44;
    authMessage.set(challenge, this.initialMessage.length + 1);
    authMessage[this.initialMessage.length + 1 + challenge.length] = 44;
    authMessage.set(
      clientMessageBare,
      this.initialMessage.length + 1 + challenge.length + 1
    );

    const clientSignature = await crypto.subtle.sign(
      { name: "HMAC" },
      await crypto.subtle.importKey(
        "raw",
        storedKey,
        { name: "HMAC", hash: this.hashType },
        false,
        ["sign"]
      ),
      authMessage
    );

    const xor1 = new Uint8Array(clientKey);
    const xor2 = new Uint8Array(clientSignature);
    const clientProof = new Uint8Array(xor1.length);
    for (let i = 0; i < xor1.length; i++) {
      clientProof[i] = xor1[i] ^ xor2[i];
    }
    const clientProofBase64 = btoa(
      String.fromCharCode(...Array.from(clientProof))
    );
    const finalMessage = new Uint8Array(
      clientMessageBare.length + 3 + clientProofBase64.length
    );
    finalMessage.set(clientMessageBare, 0);
    new TextEncoder().encodeInto(
      `,p=${clientProofBase64}`,
      finalMessage.subarray(clientMessageBare.length)
    );

    const serverKey = await crypto.subtle.sign(
      { name: "HMAC" },
      saltedPassword,
      new TextEncoder().encode("Server Key")
    );
    this.serverSignature = new Uint8Array(
      await crypto.subtle.sign(
        { name: "HMAC" },
        await crypto.subtle.importKey(
          "raw",
          serverKey,
          { name: "HMAC", hash: this.hashType },
          false,
          ["sign"]
        ),
        authMessage
      )
    );

    return finalMessage;
  }

  async verify(serverResponse: Uint8Array | null): Promise<void> {
    if (serverResponse === null) {
      throw new Error("Response expected from server");
    }
    if (!this.serverSignature) {
      throw new Error("Server signature not computed");
    }
    const decoder = new TextDecoder();
    let serverVerifier;
    const fields = decoder
      .decode(serverResponse)
      .split(",")
      .map((s) => {
        const splits = s.split("=");
        return [splits[0], splits.slice(1).join("=")];
      });
    for (const [key, value] of fields) {
      if (key === "v") {
        serverVerifier = new Uint8Array(
          atob(value)
            .split("")
            .map((ch) => ch.charCodeAt(0))
        );
      }
    }
    if (!serverVerifier) {
      throw new Error("Server sent invalid final message");
    }

    if (serverVerifier.length != this.serverSignature.length) {
      throw new Error("Server signature mismatch");
    }

    let acc = 0;
    for (let i = 0; i < this.serverSignature.length; i++) {
      acc += this.serverSignature[i] ^ serverVerifier[i];
    }
    if (acc != 0) {
      throw new Error("Server signature mismatch");
    }
  }
}

export interface RegisteredMechanism {
  mechanism: string;
  factory: (params: AuthParameters) => ISaslAuthenticator;
}

export const SUPPORTED_MECHANISMS: RegisteredMechanism[] = [
  {
    mechanism: "SCRAM-SHA-512",
    factory: (params) => new ScramAuthenticator(params, "SHA-512", 512),
  },
  {
    mechanism: "SCRAM-SHA-256",
    factory: (params) => new ScramAuthenticator(params, "SHA-256", 256),
  },
  {
    mechanism: "SCRAM-SHA-1",
    factory: (params) => new ScramAuthenticator(params, "SHA-1", 160),
  },
  { mechanism: "PLAIN", factory: (params) => new PlainAuthenticator(params) },
];

export function saslNegotiate(serverMechanisms: string[]): RegisteredMechanism {
  for (const mech of SUPPORTED_MECHANISMS) {
    if (serverMechanisms.includes(mech.mechanism)) {
      return mech;
    }
  }
  throw new Error(
    "Couldn't find any SASL mechanisms supported by both of the parties"
  );
}
