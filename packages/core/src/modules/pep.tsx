import { ComponentRegistry, MESSAGE_HANDLER } from "../plumbing/registry";
import { IBaseModule } from "../plumbing/module";

export class PepModule implements IBaseModule {
  register(registry: ComponentRegistry): void {
    registry.register(MESSAGE_HANDLER, this._handleMessage.bind(this));
  }

  private _handleMessage(elem: Element): boolean {
    for (const child of elem.children) {
      if (
        child.namespaceURI === "http://jabber.org/protocol/pubsub#event" &&
        child.localName === "event"
      ) {
        console.info("PEP event", child);
        return true;
      }
    }
    return false;
  }
}
