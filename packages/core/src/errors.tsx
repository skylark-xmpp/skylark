import { SkylarkJSX } from ".";

const STANZA_ERROR_TYPES = [
  "auth",
  "cancel",
  "continue",
  "modify",
  "wait",
] as const;

const STANZA_ERROR_CONDITIONS = [
  "bad-request",
  "conflict",
  "feature-not-implemented",
  "forbidden",
  "gone",
  "internal-server-error",
  "item-not-found",
  "jid-malformed",
  "not-acceptable",
  "not-allowed",
  "not-authorized",
  "policy-violation",
  "recipient-unavailable",
  "redirect",
  "registration-required",
  "remote-server-not-found",
  "remote-server-timeout",
  "resource-constraint",
  "service-unavailable",
  "subscription-required",
  "undefined-condition",
  "unexpected-request",
] as const;

export type StanzaErrorType = (typeof STANZA_ERROR_TYPES)[number];
export type StanzaErrorCondition = (typeof STANZA_ERROR_CONDITIONS)[number];

export class StanzaError extends Error {
  constructor(
    public type: StanzaErrorType,
    public condition: StanzaErrorCondition
  ) {
    super(`${condition} (${type})`);
    this.name = "StanzaError";
  }

  static fromXML(element: Element): StanzaError {
    let type = element.getAttribute("type") as StanzaErrorType;
    if (!type || !STANZA_ERROR_TYPES.includes(type)) {
      type = "cancel";
    }
    let condition: StanzaErrorCondition | undefined;
    for (const child of element.children) {
      if (
        child.namespaceURI === "urn:ietf:params:xml:ns:xmpp-stanzas" &&
        (STANZA_ERROR_CONDITIONS as readonly string[]).includes(child.localName)
      ) {
        condition = child.localName as StanzaErrorCondition;
      }
    }
    if (!condition) {
      condition = "undefined-condition";
    }
    return new StanzaError(type, condition);
  }

  toXML(): SkylarkJSX.Element {
    const conditionElement = SkylarkJSX.createElement(this.condition, {
      xmlns: "urn:ietf:params:xml:ns:xmpp-stanzas",
    });
    return (
      <error xmlns="jabber:client" type={this.type}>
        {conditionElement}
      </error>
    );
  }
}
