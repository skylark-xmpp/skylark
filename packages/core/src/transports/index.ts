import { WebSocketTransport } from "./websocket";

export class TransportError extends Error {
  constructor(message?: string, options?: ErrorOptions) {
    super(message, options);
    this.name = "TranportError";
  }
}

export interface ITransportLogger {
  log(direction: "in" | "out", payload: string): void;
}

export interface IXmppTransport {
  transportLogger?: ITransportLogger;
  onStanzaReceived?: (stanza: Element) => void;
  onStreamClosed?: () => void;

  openStream(toDomain: string): Promise<void>;
  closeStream(): Promise<void>;

  sendStanza(xmlStanza: string): Promise<void>;
}

export { WebSocketTransport };
