type DOMElement = Element;

const globalSerializer = new XMLSerializer();

export namespace SkylarkJSX {
  export declare namespace JSX {
    interface Element {
      name: string;
      props: Record<string, any>;
      children: (string | Element)[];
    }

    interface IntrinsicElements {
      [name: string]: any;
    }
  }

  export function createElement(
    name: string,
    props: Record<string, any>,
    ...children: (string | Element)[]
  ): JSX.Element {
    return { name, props, children };
  }

  export type Element = JSX.Element;

  export function toDOM(doc: XMLDocument, el: Element): DOMElement {
    return toDOMInternal(doc, el);
  }

  export function toXml(el: Element): string {
    const doc = document.implementation.createDocument(null, null);
    doc.appendChild(toDOM(doc, el));
    return globalSerializer.serializeToString(doc);
  }

  function toDOMInternal(
    doc: XMLDocument,
    el: Element,
    namespace?: string
  ): DOMElement {
    if (el.props && el.props["xmlns"]) {
      namespace = el.props["xmlns"];
    }
    let domEl;
    if (namespace) {
      domEl = doc.createElementNS(namespace, el.name);
    } else {
      domEl = doc.createElement(el.name);
    }
    for (const prop in el.props) {
      if (prop === "xmlns") {
        continue;
      }
      if (typeof el.props[prop] === "boolean" && !el.props[prop]) {
        continue;
      }
      if (el.props[prop] === undefined || el.props[prop] === null) {
        continue;
      }
      domEl.setAttribute(prop, el.props[prop]);
    }
    if (el.children === undefined) console.log(el);
    for (const child of el.children) {
      if (Array.isArray(child)) {
        for (const subchild of child) {
          domEl.appendChild(toDOMInternal(doc, subchild, namespace));
        }
      } else if (typeof child === "object") {
        domEl.appendChild(toDOMInternal(doc, child, namespace));
      } else {
        domEl.append(child);
      }
    }
    return domEl;
  }
}
