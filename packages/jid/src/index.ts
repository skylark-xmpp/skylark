export class JidParseError extends Error {
  constructor(message: string) {
    super(message);
    this.name = "JidParseError";
  }
}

// XMPP JID parsing and manipulation according to https://datatracker.ietf.org/doc/html/rfc7622
export class JID {
  local: string | null;
  domain: string;
  resource: string | null;

  constructor(fromString: string) {
    const [prefix, resource] = fromString.split("/", 2);

    if (prefix.length === 0) {
      throw new JidParseError("JID must at least have a domainpart");
    }

    let local, domain;
    const prefixParts = prefix.split("@", 2);
    if (prefixParts.length === 1) {
      domain = prefixParts[0];
    } else {
      [local, domain] = prefixParts;
    }

    if (domain.length < 1 || domain.length > 1023) {
      throw new JidParseError(
        "The length of JID domainpart must be between 1 and 1023 characters"
      );
    }

    if (local !== undefined && (local.length < 1 || local.length > 1023)) {
      throw new JidParseError(
        "The length of JID localpart must be between 1 and 1023 characters"
      );
    }

    if (
      resource !== undefined &&
      (resource.length < 1 || resource.length > 1023)
    ) {
      throw new JidParseError(
        "The length of JID resourcepart must be between 1 and 1023 characters"
      );
    }

    this.domain = domain;
    this.local = local !== undefined ? local : null;
    this.resource = resource !== undefined ? resource : null;
  }

  toBareString(): string {
    if (this.local) {
      return `${this.local}@${this.domain}`;
    } else {
      return this.domain;
    }
  }

  toString(): string {
    if (this.resource) {
      return `${this.toBareString()}/${this.resource}`;
    } else {
      return this.toBareString();
    }
  }
}
