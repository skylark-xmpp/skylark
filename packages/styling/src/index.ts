export interface PlainBlock {
  type: "plain";
  spans: TextSpan[];
}

export interface UnformattedBlock {
  type: "pre" | "me";
  content: string;
}

export interface QuoteBlock {
  type: "quote";
  children: TextBlock[];
}

export type TextBlock = PlainBlock | UnformattedBlock | QuoteBlock;

export interface PlainSpan {
  type: "plain" | "pre";
  text: string;
}

export interface FormattedSpan {
  type: "em" | "strong" | "strike";
  spans: TextSpan[];
}

export type TextSpan = PlainSpan | FormattedSpan;

export function parseToSpans(line: string): TextSpan[] {
  return parseToSpansInternal(line, 0, line.length);
}

function parseToSpansInternal(
  line: string,
  parseFrom: number,
  parseUntil: number
): TextSpan[] {
  const CHAR_MAP: Record<string, "em" | "strong" | "strike"> = {
    _: "em",
    "*": "strong",
    "~": "strike",
  };
  const spans: TextSpan[] = [];
  const chars = line.split("");
  let lastSpanEnd = 0;
  let activeSpan = null as null | {
    ty: string;
    start: number;
  };
  for (let i = parseFrom; i < parseUntil; i++) {
    const matchesChar = /[_*~`]/.test(chars[i]);
    const isWordStart = i + 1 < parseUntil && chars[i + 1] !== " ";
    const isWordEnd = i > parseFrom && chars[i - 1] !== " ";
    if (matchesChar && isWordStart && !activeSpan) {
      activeSpan = { ty: chars[i], start: i };
    } else if (
      activeSpan &&
      matchesChar &&
      chars[i] === activeSpan.ty &&
      isWordEnd
    ) {
      if (i - activeSpan.start - 1 === 0) {
        // ignore empty spans
        activeSpan = null;
        continue;
      }

      if (activeSpan.start - lastSpanEnd > 0) {
        spans.push({
          type: "plain",
          text: chars.slice(lastSpanEnd, activeSpan.start).join(""),
        });
      }

      if (activeSpan.ty === "`") {
        // Preformatted spans get special treatment
        spans.push({
          type: "pre",
          text: chars.slice(activeSpan.start, i + 1).join(""),
        });
      } else {
        const inner = chars.slice(activeSpan.start, i + 1).join("");
        spans.push({
          type: CHAR_MAP[activeSpan.ty],
          spans: parseToSpansInternal(inner, 1, inner.length - 1),
        });
      }

      activeSpan = null;
      lastSpanEnd = i + 1;
    }
  }
  if (lastSpanEnd < line.length) {
    spans.push({
      type: "plain",
      text: chars.slice(lastSpanEnd).join(""),
    });
  }
  return spans;
}

function parseLines(
  lines: string[],
  isRootBlock: boolean = false
): TextBlock[] {
  const blocks: TextBlock[] = [];
  let i = 0;
  while (i < lines.length) {
    if (lines[i].startsWith("```")) {
      // Start of preformatted text block
      let end = i + 1;
      while (end < lines.length && lines[end] !== "```") end++;
      blocks.push({
        type: "pre",
        content: lines.slice(i + 1, end).join("\n"),
      });
      i = end + 1;
    } else if (lines[i].startsWith(">")) {
      let end = i + 1;
      while (end < lines.length && lines[end].startsWith(">")) end++;
      const quoteContent = lines
        .slice(i, end)
        .map((l) => l.slice(1).trimStart());
      blocks.push({
        type: "quote",
        children: parseLines(quoteContent),
      });
      i = end;
    } else if (isRootBlock && lines[i].startsWith("/me ")) {
      blocks.push({
        type: "me",
        content: lines[i].slice(4).trim(),
      });
      i += 1;
    } else {
      blocks.push({
        type: "plain",
        spans: parseToSpans(lines[i]),
      });
      i += 1;
    }
  }
  return blocks;
}

export function parse(text: string): TextBlock[] {
  const lines = text.split(/\r?\n/);
  return parseLines(lines, true);
}
