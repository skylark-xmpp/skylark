const defaultTheme = require("tailwindcss/defaultTheme");
const colors = require("tailwindcss/colors");

/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{vue,ts}"],
  theme: {
    borderRadius: {
      DEFAULT: "16px",
      sm: "8px",
      full: defaultTheme.borderRadius["full"],
    },
    extend: {
      backgroundColor: {
        primary: colors.neutral[900],
        secondary: colors.neutral[800],
        highlight: colors.neutral[700],
      },
      textColor: {
        DEFAULT: colors.neutral[300],
        dim: colors.neutral[400],
      },
      borderColor: {
        DEFAULT: colors.neutral[800],
      },
      fontFamily: {
        sans: ["Poppins", ...defaultTheme.fontFamily.sans],
      },
    },
  },
  plugins: [],
};
