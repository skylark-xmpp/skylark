import { acceptHMRUpdate, defineStore } from "pinia";

export interface DebugEntry {
  type: "xmlOut" | "xmlIn";
  timestamp: Date;
  contents: string;
}

export const useDebugStore = defineStore("debug", {
  state: () => ({
    entries: [] as DebugEntry[],
  }),
  actions: {
    pushEntry(entry: DebugEntry) {
      this.entries.push(entry);
    },
  },
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useDebugStore, import.meta.hot));
}
