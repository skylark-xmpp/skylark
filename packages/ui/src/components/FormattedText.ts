import { TextBlock, TextSpan, parse } from "@skylark/styling";
import { VNode, h } from "vue";
import style from "./FormattedText.module.css";

type Props = {
  rawText: string;
  userNick: string;
};

function spanToElement(span: TextSpan): VNode | string | undefined {
  if (span.type === "pre") {
    return h("code", {}, span.text);
  } else if (span.type === "plain") {
    return span.text;
  } else if (span.type === "em") {
    return h(
      "em",
      {},
      span.spans.map((span) => spanToElement(span))
    );
  } else if (span.type === "strong") {
    return h(
      "strong",
      {},
      span.spans.map((span) => spanToElement(span))
    );
  } else if (span.type === "strike") {
    return h(
      "s",
      {},
      span.spans.map((span) => spanToElement(span))
    );
  }
}

function blockToElement(userNick: string, block: TextBlock): VNode | undefined {
  if (block.type === "pre") {
    return h("pre", {}, block.content);
  } else if (block.type === "quote") {
    return h(
      "blockquote",
      {},
      block.children.map((block) => blockToElement(userNick, block))
    );
  } else if (block.type === "plain") {
    return h(
      "p",
      {},
      block.spans.map((span) => spanToElement(span))
    );
  } else if (block.type === "me") {
    return h("p", {}, [h("em", {}, ["* ", userNick, " ", block.content])]);
  }
}

export function FormattedText(props: Props) {
  const parsed = parse(props.rawText);
  return h(
    "div",
    {
      class: style.formattedText,
    },
    parsed.map((block) => blockToElement(props.userNick, block))
  );
}

FormattedText.props = {
  rawText: {
    type: String,
    required: true,
  },
  userNick: {
    type: String,
    required: true,
  },
};
