import { openDB, IDBPDatabase } from "idb";

export interface Conversation {
  jid: string;
  lastActive: number;
}

export interface Message {
  conversation: string;
  timestamp: number;
  payload: string;
  decryptedEnvelope?: string;
}

export interface OMEMOKeypair {
  id: string;
  type: "ed25519" | "x25519";
  keypair: Uint8Array;
  pubkeySignature?: Uint8Array;
  active: boolean;
  generatedAt: number;
}

export class UserDB {
  _db: IDBPDatabase;

  private constructor(db: IDBPDatabase) {
    this._db = db;
  }

  static async openForJid(jid: string) {
    const db = await openDB(`skylark_userdb:${jid}`, 1, {
      upgrade(db, _oldVersion, _newVersion, _transaction, _event) {
        db.createObjectStore("settings", { keyPath: "key" });
        db.createObjectStore("conversations", { keyPath: "jid" });
        const messagesStore = db.createObjectStore("messages", {
          autoIncrement: true,
        });
        messagesStore.createIndex("conversation_idx", "conversation");
        db.createObjectStore("omemo_keypairs", { keyPath: "id" });
      },
    });
    return new UserDB(db);
  }

  close() {
    this._db.close();
  }

  async getSetting(key: string): Promise<any> {
    const row = await this._db.get("settings", key);
    return row?.value;
  }

  async setSetting(key: string, value: any): Promise<void> {
    await this._db.put("settings", { key, value });
  }

  async storeMessage(msg: Message): Promise<void> {
    this._db.put("conversations", {
      jid: msg.conversation,
      lastActivity: Date.now(),
    });
    await this._db.add("messages", msg);
  }

  async getKnownConversationJids(): Promise<string[]> {
    return (await this._db.getAllKeys("conversations")) as string[];
  }

  async retrieveMessages(conversation: string): Promise<Message[]> {
    return await this._db.getAllFromIndex(
      "messages",
      "conversation_idx",
      conversation
    );
  }

  async getOMEMOKeypairs(): Promise<OMEMOKeypair[]> {
    return await this._db.getAll("omemo_keypairs");
  }

  async updateOMEMOKeypair(keypair: OMEMOKeypair): Promise<void> {
    await this._db.put("omemo_keypairs", keypair);
  }
}
