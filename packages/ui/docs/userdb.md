# conversations
| Field name | Description |
| ---------- | ----------- |
| jid | JID of the other party or the chatroom |
| lastActive | Time of last message received or sent (Unix time in milliseconds) |

# messages
| Field name | Description |
| ---------- | ----------- |
| conversation | JID of the conversation |
| timestamp | Time of message |
| payload | The `<message/>` stanza serialized as XML |
| decryptedEnvelope | Decrypted SCE envelope as XML |

# omemo_keypairs
| Field name | Description |
| ---------- | ----------- |
| id | Keypair ID (see [ID formats](#id-formats)) |
| type | `ed25519` or `x25519` |
| keypair | Keypair as serialized by `wasmcrypto` |
| pubkeySignature | EdDSA signature of the public key (only for signed prekeys) |
| active | Whether the keypair should be actively published |
| generatedAt | Generation timestamp as Unix milliseconds |

## ID formats
| Keypair | ID format |
| ---------- | ----------- |
| Identity key | `ik` |
| Signed prekey | `spk:<id>` |
| One-time prekey | `opk:<id>` |

# settings
| Field name | Description |
| ---------- | ----------- |
| key | Setting key (see [Known settings](#known-settings)) |
| value | Setting value |

## Known settings
| Key | Description |
| --- | ----------- |
| `omemo:device_id` | OMEMO device ID |